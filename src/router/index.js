import { createRouter, createWebHistory } from "vue-router";
// import HomePage from "../view/HomePage.vue";
// import AboutPage from "../view/AboutPage.vue";
import NotFound from "@/view/NotFound.vue";
const routes = [
  {
    path: "/",
    name: "home",
    component: () => import("@/view/HomePage.vue"),
  },
  {
    path: "/about",
    name: "about",
    component: () => import("@/view/AboutPage.vue"),
  },
  {
    path: "/user",
    name: "user",
    component: () => import("@/view/UsersPage.vue"),
  },
  {
    path: "/user/:id",
    name: "UserSingle",
    component: () => import("@/view/UsersSinglePage.vue"),
    props:true
  },
  {
    path: "/member",
    redirect: "/about",
  },
  {
    path: "/:pathMatch(.*)*",
    name: "NOT FOUND",
    component: NotFound,
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
